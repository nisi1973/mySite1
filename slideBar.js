// Elements to be used as default in the slide Bar if not enough has been configured
module.exports = {
	defaultElements:[
			{ title: 'Castelli di Lagnasco - Castello di Levante (foto Gonella)',
    			when: '2015',
    			selectedImage: '/images/slider1.jpg',
    			language: 'IT',
    			description: '#ilcastellotifabello',
    			//id: 'Le_invasioni_digitali__26_Aprile',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg ',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		},
    		{ title: 'Castelli di Lagnasco - Sala della Giustizia (foto Gonella)',
    			when: '2015',
    			selectedImage: '/images/slider2.jpg',
    			language: 'IT',
    			description: '#ilcastellotifabello',
    			//id: 'Le_invasioni_digitali__26_Aprile',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		},
    		{ title: 'Castelli di Lagnasco - Il sogno del Fauno (foto Gonella)',
    			when: '2015',
    			selectedImage: '/images/slider3.jpg',
    			language: 'IT',
    			description: '#ilcastellotifabello',
    			//id: 'Le_invasioni_digitali__26_Aprile',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		},
    		{ title: 'Castelli di Lagnasco - Concerto (foto Gonella)',
    			when: '2015',
    			selectedImage: '/images/slider4.jpg',
    			language: 'IT',
    			description: '#ilcastellotifabello',
    			//id: 'Le_invasioni_digitali__26_Aprile',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		},
    		{ title: 'Castelli di Lagnasco - Castello di Levante (foto Gonella)',
    			when: '2015',
    			selectedImage: '/images/slider5.jpg',
    			language: 'IT',
    			description: '#ilcastellotifabello',
    			//id: 'Le_invasioni_digitali__26_Aprile',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		}		
		]
}