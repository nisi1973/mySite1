/**
 * f.invernizzi@libero.it 2015.
 *
 * Castelli di Lagnasco
 * 
 * Generazione dinamica di contenuti, news
 *
 * All right reserved f.invernizzi@libero.it
 * 
 */

/****************************
 *
 *  SETTINGS and CONSTANTS
 *
 ***************************/
 
// configuration file
var configuration = require("./config.js");

process.name = configuration.main.processName;

var pmx = require('pmx').init();
pmx.http();

var express = require('express'),
    _ = require("lodash"),
	bodyParser = require('body-parser'),
	//redis = require("redis"),
	util = require('util'),
	async = require('async'),
	sha1 = require('sha-1'),
	multer  = require('multer'),
	fs = require("fs"),
	utils = require("./lib/util.js"),
	backend = require('./lib/backend.js'),
	createRotatingStream = require('file-stream-rotator'),
	errorDetails = require('./errors.js'),
	compress = require('compression'),
	handlebars = require("express3-handlebars").create({defaultLayout: 'main'})
	Log = require('log'),
    log = new Log('debug', fs.createWriteStream(configuration.main.logFile));      
var app = express();

// handlebars templating engine
app.engine('handlebars' , handlebars.engine);
app.set('view engine' , 'handlebars');
app.set("name" , configuration.main.processName);
/******************************************************/
// DEVELOPMENT - PRODUCTION
// This changes also where libraries are loaded from
utils.setEnv(configuration.main.mode , app);
/******************************************************/
// GZIP compression
app.use(compress());  

switch(app.get('env')){
    case 'development':
    	console.log("WORKING IN DEVELOPMENT MODE\n");
        console.log("Static content dir:"+__dirname+configuration.content.STATIC_CONTENT_DIR);
        app.use(require('morgan')('dev'));
        break;
    case 'production':
    	// ensure log directory exists
		fs.existsSync(configuration.main.logDir) || fs.mkdirSync(configuration.main.logDir)
		
		// create a rotating write stream
		var accessLogStream = createRotatingStream.getStream({
  			filename: configuration.main.logDir + '/castello_access-%DATE%.log',
  			frequency: configuration.main.logFileRotationFrequency,
  			verbose: false
		});
    	app.use(require('morgan')('combined' , {stream: accessLogStream}));
        break;
}

app.use(bodyParser.json());


app.listen(configuration.main.LISTENPORT);
console.log()
console.log("------------------------------------------------------");
console.log("       Working in "+app.get("env") + " mode");
console.log("------------------------------------------------------");
console.log()
console.log("Server listening on "+configuration.main.LISTENPORT);



/***********************************************************************************************************************
 *
 *  ROUTING
 *
 ***********************************************************************************************************************/

/**
 * STATIC CONTENT
 */
app.use(express.static(__dirname+configuration.content.STATIC_CONTENT_DIR));

// POST data
//app.use(bodyParser.json());
// Needed for browseImage post 
app.use(bodyParser.urlencoded({ extended: false }))

// Show the short page of a news
app.get("/news_short/:id/:lan" , function (req, res) {
	var language = req.params.lan || configuration.LANG;
	if (!req.params.id){
		resNotFound(res);
	}else{
		newsDetails(utils.fullID(req.params.id,language), function(error , replies){
			if (error || ! replies)
				serverError(res);
			else{
				replies = utils.newsStaticInfo(replies);
				replies.id = req.params.id;
				res.render("news_short" , replies);
			}
		});
	}
});
// Show the complete page of a news
app.get("/news/:id/:lan" , function (req, res) {
	var language = req.params.lan || configuration.LANG;
	if (!req.params.id){
		resNotFound(res);
	}else{
		backend.newsDetails(utils.fullID(req.params.id,language), function(error , replies){
			if (!replies){
				log.error("News not found: "+utils.fullID(req.params.id,language));
				resNotFound(res);
			}else{
				replies = utils.newsStaticInfo(replies);
				res.render("news" , replies);
			}
		});
	}
});

// list of active news
app.get("/news_list" , function (req, res) {
	backend.newsList(function(err, list){
		backend.newsDetailsList(list, function(error , list){
			if (error)
				serverError(res);
			else{
				// Json format or the HTML page?
				if (req.query.format == 'json'){
					res.setHeader('Content-Type', 'application/json');
						res.send(JSON.stringify(list));
					}else
					res.render("news_list" , {"news":list , production:(app.get('env') == "production")});
			}
		});
	});
});


// list of active news
// The returned asnwer is json formated
app.get("/news_mobile_app_list" , function (req, res) {
        backend.newsMobileAppList(function(err, list){
		var list_full_ID = [];
		for (var i=0; i<list.length; i++)
			list_full_ID.push(utils.fullID(list[i],req.query.lang || 'IT'));
                backend.newsDetailsList(list_full_ID, function(error , list){
                        if (error)
                                serverError(res);
                        else{
                                        res.setHeader('Content-Type', 'application/json');
                                        res.send(JSON.stringify(list));
                        }
                });
        });
});
// list of first page news
// Used by the app to publish news
app.get("/first_page_news" , function (req, res) {
	var lang = req.query.lang || 'it';
        backend.firstPageNewsInfo(lang, function(err, list){
                        if (err)
                             serverError(res);
                        else{
                              // Json format or the HTML page?
                              if (req.query.format == 'json'){
                                     res.setHeader('Content-Type', 'application/json');
                                             res.send(JSON.stringify(list));
                                      }else
                                      res.render("first_page_news" , {"news":list , production:(app.get('env') == "production")});
                        }

        });
});

//--------------
// First page
//--------------
app.get(["/index.html/:lan"  , "/index*", "/" ], function (req, res) {
	var language = req.params.lan || configuration.LANG;
	if (!utils.acceptedLanguage(language)){
		log.error("Language not accepted:"+language);
		serverError(res);
		return false;
	}
	// First page news - spalla (sidebars)
	backend.firstPageNewsInfo(language , function(err , info){
		if (err){
			log.err(err);
			serverError(res);
			return false;
		}	
		// Add static info read from config file
		info = {
			sideBar:info.sideBar,
			slideBar:info.slideBar,
			title: configuration.META.title,
			description: configuration.META.description,
			image: configuration.META.image,
			url: configuration.META.url,
			isIndex:true
		};
		info = utils.newsStaticInfo(info);
		res.render("index" , info);
	});
});

// Gallery page
app.get("/gallery.html" , function(req , res){
	var context={};
	context = backend.gallery();
	context = utils.newsStaticInfo(context);
	res.render("gallery" , context);
});

// Get json description of galleries
app.get("/getGalleries" , function(req , res){
	res.send(backend.gallery());
});


// Any html request is rendered by means of handlebars
// We extract the name of the requested page without html, and try to render it
app.get("/*.html", function (req, res) {
	var urlEl = req.url.split("/");
	var u = urlEl[urlEl.length-1].split(".");
	// index has a different structure
	res.render(u[0] , {production:(app.get('env') == "production") , isIndex:(u[0] == "index")});
});

/**
 * Missing route
 */
app.use(function(req , res){
    resNotFound(res);
});

/**
 * SERVER ERROR
 */
app.use(function(err , req , res , next){
    serverError(res);
});

app.use(pmx.expressErrorHandler());



// Replies with a not found page
function resNotFound(res){
    res.status(404);
    var responseContext =  errorDetails["notFound"];
    responseContext.description = utils.shortenDescription(responseContext.description);
    responseContext.production = (app.get('env') == "production");
    res.render("news_short" ,responseContext);
}
// Server Error
function serverError(res){
    res.status(500);
    var responseContext =  errorDetails["serverError"];
    responseContext.description = utils.shortenDescription(responseContext.description);
    responseContext.production = (app.get('env') == "production");
    res.render("news_short" ,responseContext);
}


