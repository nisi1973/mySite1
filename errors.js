// Errors details
module.exports = {
	notFound:{ title: 'Elemento non trovato',
    			when: '2015',
    			selectedImage: '/images/logo.png',
    			language: 'IT',
    			description: 'Ops... non abbiamo trovato quello che cercavi ... se vuoi puoi chiamare Momby al 335610610610',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg ',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		},
    serverError:{ title: 'Server Error',
    			when: '2015',
    			selectedImage: '/images/logo.png',
    			language: 'IT',
    			description: 'Ops... Huston abbiamo un problema...',
    			video: '',
    			pageUrl: 'http://www.castellidilagnasco.it',
    			image: 'http://www.castellidilagnasco.it/images/slider1.jpg ',
    			url: 'http://www.castellidilagnasco.it/news/Le_invasioni_digitali__26_Aprile/IT',
    			isVideo: false 
    		}
}