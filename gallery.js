// Definizione delle gallery
module.exports = {
		  gallery:[
		  {	
		  	id:"gallery_1",
		  	title:"Le sale interne",
		  	visible: true,
		  	elements:[
		  		{ 
					title: 'Castelli di Lagnasco - Sala degli scudi (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/20142201125.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Il sogno del Fauno (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220113412.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Castello di Ponente (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/20142201134.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Sala delle Dame (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011102.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Sala della Giustizia (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011147.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Cantine affrescate (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011220.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Sala della Giustizia (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011251.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Sala della Giustizia (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011318.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Castello di Ponente (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011332.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Sala delle Dame (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011415.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Castello di Ponente (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011852.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Cantine affrescate (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011910.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Sala della Giustizia (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/201422011947.jpg',
    				isVideo: false   
				}
				
		  		] //elements
		  	},
		  	{	
		  	id:"gallery_2",
		  	title:"Gli esterni",
		  	elements:[
		  		{ 
					title: 'Castelli di Lagnasco - Torre Nord-Est (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102525.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Ingresso Nord (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102540.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Giardini Nord (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102554.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Torre Nord-Est (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102612.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Loggiato di Levante (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102643.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Castello di Ponente (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102748.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Ingresso Nord (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102822.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Castello di Levante, cortile (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/2014220102842.jpg',
    				isVideo: false   
				}
				
				
				
				
		  		] //elements
		  	},
		  	{	
		  	id:"gallery_3",
		  	title:"Eventi in Castello",
		  	elements:[
		  		{ 
					title: 'Castelli di Lagnasco - Rievocazione storica (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_3331.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Rievocazione storica (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_3250.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Rievocazione storica (foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_7772.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Concerto nei cortili(foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_2648.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Concerto nei cortili(foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_8102.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Mercatini di Natale(foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_2050.jpg',
    				isVideo: false   
				},
				{ 
					title: 'Castelli di Lagnasco - Mercatini di Natale(foto Gonella)',
    				language: 'IT',
    				video: '',
    				image: 'http://www.castellidilagnasco.it/photogallery/IMG_2041.jpg',
    				isVideo: false   
				}
				
		  		] //elements
		  	}
		  	
		  	]// gallery
}