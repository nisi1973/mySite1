#!/usr/bin/node

// configuration file
var configuration = require("./config.js"),
	backend = require('./lib/backend.js'),
	handlebars = require("handlebars"),
	fs = require('fs'), 
	Log = require('log'),
	async = require('async'),
	_ = require("lodash"),
	nodemailer = require("nodemailer"),
	smtpTransport = require('nodemailer-smtp-transport'),
	cli = require('cli');
	
// CLI options	
var options = cli.parse({
    justCheck:   ['c', 'Only checks mails to be sent' , "bool",false]
});

// Open log file
try{
	var log = new Log('debug', fs.createWriteStream(configuration.mailAgent.logFile));
}catch(e){
	// If we have an error opening the log file, we cannot log to it... use console.log
	console.log("Error opening log file in Mail agent!");
	process.exit(1);
}

// Daemon mode or oneShot?
if ((configuration.mailAgent.mode == "daemon") && !options.justCheck) {
	setInterval(checkMail, configuration.mailAgent.checkPeriod);
}else{
	checkMail();
}

// OK, lets prepare everything for sending emails
var transporter = nodemailer.createTransport(smtpTransport({
   		host: configuration.mailAgent.host,
    	port: configuration.mailAgent.port,
    	name : configuration.mailAgent.name,
    	secure: true,
    	auth: {
        	user: configuration.mailAgent.auth.user,
        	pass: configuration.mailAgent.auth.pass
    	},
    	tls: {
        	rejectUnauthorized:false
    	}
}));

// Handlebars template to be used in mails
var mailTemplate = fs.readFileSync(configuration.mailAgent.template , {encoding : "utf8"});

function checkMail(){
	log.info("Checking for mails to be sent");
	// Load distribution lists and news info and send them
    backend.getMailsToBeSent(function(err , details){
	if (err){
		log.critical(err);
	}else{
		var DHs = _.keys(details);
		// We should work with async for closing in the right way
		async.each(DHs, function(dh , callback){
			var template = handlebars.compile(mailTemplate);
			var htmlContent = "";
			try{
				if (!details[dh].news){
					log.error("Error on a news in "+dh);
					return callback(new Error("Error on a news in "+dh));
				}
				details[dh].news.host = "http://www.castellidilagnasco.it";
				htmlContent = template(details[dh].news);
				if (!details[dh].news || !details[dh].news.title){
					callback(new Error("Missing info from news"));
				}
			}catch(e){
				log.critical(e);
				callback(e);
			}
			if (options.justCheck){
				cli.info("--------------------------------");
				cli.info("subject: "+details[dh].dl);
				cli.info("to: "+details[dh].dl);
				cli.info("text: "+details[dh].news.description);
				cli.info("--------------------------------\n");
			}else{
				// Send emails
				transporter.sendMail({
    				from: configuration.mailAgent.from,
    				to: configuration.mailAgent.to,
					bcc: details[dh].dl,
    				subject: details[dh].news.title,
					text: details[dh].news.description,
    				html: htmlContent
				} , function(err , info){
					if (err)
						log.critical(err);
					// TODO: add here a mail to admin for log purpouse
					log.info(info);
					// Even if I have errors, i remove the DH
					backend.DHsent(dh , function(err){
						backend.newsSent(details[dh].news.id , dh , function(err){
							callback(err);
						});
					});
				});
			} // Else send if not justCheck
			
		},  // Iterator
		function(err){
    		// FINAL TASKS
			backend.end();
    	});
	}// Else (not error)
 });
}




