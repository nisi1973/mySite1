/**
 * f.invernizzi@libero.it 2015.
 *
 * Castelli di Lagnasco
 * 
 * Generazione dinamica di contenuti, news
 *
 * All right reserved f.invernizzi@libero.it
 *
 * 16012017 - Supporto per flag MobileApp
 * 
 */

/****************************
 *
 *  SETTINGS and CONSTANTS
 *
 ***************************/
 
// configuration file
var configuration = require("./config.js");
 
// TODO: For now we keep internal var.
// We should remove them and use directly the configuration obj
// To be managed with cookies
var LANG = configuration.LANG; // Default language
var IMG = configuration.IMG;

var NOT_FOUND_NEWS_ID = configuration.NOT_FOUND_NEWS_ID; // If not found, use this news id
var DEFAULT_NEWS_ID = configuration.DEFAULT_NEWS_ID; 
var SERVER_ERROR_NEWS_ID = configuration.SERVER_ERROR_NEWS_ID; // Server error page

process.name = configuration.main.processName;

var express = require('express'),
	//https = require('https'),
	https = require('http'),
    _ = require("lodash"),
	bodyParser = require('body-parser'),
	//redis = require("redis"),
	util = require('util'),
	async = require('async'),
	sha1 = require('sha-1'),
	multer  = require('multer'),
	fs = require("fs"),
	utils = require("./lib/util.js"),
	backend = require('./lib/backend.js')
	auth = require('http-auth'),
	createRotatingStream = require('file-stream-rotator'),
	handlebars = require("express3-handlebars").create({defaultLayout: 'main'});      

var app = express();

// handlebars templating engine
app.engine('handlebars' , handlebars.engine);
app.set('view engine' , 'handlebars');

/******************************************************/
// DEVELOPMENT - PRODUCTION
// This changes also where libraries are loaded from
utils.setEnv(configuration.main.mode , app);
/******************************************************/

app.set("name" , configuration.main.processName+"_MANAGEMENT");
app.use(auth.connect(auth.basic({
    realm: configuration.management.realm,
    file: configuration.management.file // gevorg:gpass, Sarah:testpass ... 
})));

switch(app.get('env')){
    case 'development':
    	console.log("WORKING IN DEVELOPMENT MODE\n");
        console.log("Static content dir:"+__dirname+configuration.content.STATIC_CONTENT_DIR);
        app.use(require('morgan')('dev'));
        break;
    case 'production':
    	// ensure log directory exists
		fs.existsSync(configuration.management.logDir) || fs.mkdirSync(configuration.management.logDir)

		// create a rotating write stream
		var accessLogStream = createRotatingStream.getStream({
  			filename: configuration.management.logDir + '/castello_management_access-%DATE%.log',
  			frequency: 'daily',
  			verbose: false
		});
    	app.use(require('morgan')('combined' , {stream: accessLogStream}));
        break;
}

app.use(bodyParser.json());
/*Configure the multer. Image upload*/
app.use(multer(
   { dest: configuration.UPLOADS_DIR,
 	rename: function (fieldname, filename) {
    	return _.kebabCase(filename)+"_"+Date.now();
  	},
	onFileUploadStart: function (file) {
	},
	onError: function(error){
		res.json(
    		{
    			"files": [
    			{
    				"name": "",
    				"size": "",
    				"url": "",
    				"thumbnailUrl": "",
    				"error": error.ToString()
  				}]
  			}
  		);
	},
	onFileUploadComplete: function (file , req, res) {
		// Response format: https://github.com/blueimp/jQuery-File-Upload/wiki/Setup#using-jquery-file-upload-ui-version-with-a-custom-server-side-upload-handler
  	  res.json(
    		{
    			"files": [
    			{
    				"name": file.name,
    				"size": file.size,
    				"url": "/uploads/"+file.name,
    				"thumbnailUrl": "/uploads/"+file.name
  				}]
  			}
  		);
	}
}));

/*
https.createServer({
 		key: fs.readFileSync(configuration.management.sslKey),
  		cert: fs.readFileSync(configuration.management.cert)
	}, app).listen(configuration.management.LISTENPORT);
*/

https.createServer(app).listen(configuration.management.LISTENPORT);

console.log()
console.log("------------------------------------------------------");
console.log("      MANAGER - Working in "+app.get("env") + " mode");
console.log("------------------------------------------------------");
console.log()
console.log("Server listening on "+configuration.management.LISTENPORT);



/***********************************************************************************************************************
 *
 *  ROUTING
 *
 ***********************************************************************************************************************/

/**
 * STATIC CONTENT
 */
app.use(express.static(__dirname+configuration.content.STATIC_CONTENT_DIR));

// POST data
//app.use(bodyParser.json());
// Needed for browseImage post 
app.use(bodyParser.urlencoded({ extended: false }))

//---------------------------------------------------------------------------------------
// --- ADMIN ---
app.get("/admin/*.html", function (req, res) {
	// Defines the options to be used by handlebars
	var opt = {layout : "admin" , production:(app.get('env') == "production")}
	var urlEl = req.url.split("/");
	var u = urlEl[urlEl.length-1].split(".");
	// In alcun casi servono informazioni di contesto specifiche per la pagina
	switch (u[0]){
		case "manageNews":
			// Mailing lists
			opt.MLs = [];
			backend.DLs(function(err , mls){
				mls.forEach(function(val , index){
					var details = val.split(":");
					opt.MLs.push({
						name:details[1],
						id : val
					});
				});
			});
			break;
		case "browseImages":
			// Serve l'elenco delle immagini da cui scegliere
			opt.imgFiles = fs.readdirSync(configuration.UPLOADS_DIR);
			// Servono solo le info, non tutto il contesto
			opt.layout = null; 
			break;
		default:
				
	}
	res.render(u[0] , opt);
});

// News update
app.get("/admin/update_news/", function (req, res) {
	backend.updateNews(req.query);
	res.send("ok");
});

// Add sideBar
app.get("/admin/set_sideBar/:newsID/:which/:rand", function (req, res) {
	backend.setSeideBarNews(req.params.newsID , req.params.which);
	res.send("ok");
});

// Add content to slideBar
app.get("/admin/news_to_slidebar/:newsId" , function(req,res){
	backend.addNewsToSlideBar(req.params.newsId , function(err , ret){
		if (err)
			serverError(res , err);
		else{
			res.send("ok");
		}	
	});
});

// Remove a news to slideBar
app.get("/admin/remove_from_slidebar/:newsId" , function(req,res){
	backend.remNewsFromSlideBar(req.params.newsId , function(err , ret){
		if (err)
			serverError(res , err);
		else{
			res.send("ok");
		}	
	});
});

// List of contents in slideBar (excluding default ones)
app.get("/admin/news_in_slidebar/:lang" , function(req,res){
	backend.newsInSidebar(null , function(err , ret){
		if (err)
			serverError(res , err);
		else{
			backend.newsDetailsList(ret , function(err,details){
				res.send(details);
			}
			, req.params.lang);
			
		}	
	});
});

// Set exactly the slidebar news as provided in post array
app.post("/admin/set_news_in_slideBar" , function(req , res){
        backend.setNewsInSlideBar(req.body.news , function(err){
                if (err)
                        res.status(500).send("Error setting Slidebar news");
                else
                        res.send("OK");
        });
});


// --- MOBILE APP ---
// Add content to MobileApp
app.get("/admin/news_to_MobileApp/:newsId" , function(req,res){
        backend.addNewsToMobileApp(req.params.newsId , function(err , ret){
                if (err)
                        serverError(res , err);
                else{
                        res.send("ok");
                }
        });
});

// Remove a news from mobile app
app.get("/admin/remove_from_MobileApp/:newsId" , function(req,res){
        backend.remNewsFromMobileApp(req.params.newsId , function(err , ret){
                if (err)
                        serverError(res , err);
                else{
                        res.send("ok");
                }
        });
});

// List of contents in MobileApp 
app.get("/admin/news_in_MobileApp/:lang" , function(req,res){
        backend.newsInMobileApp(null , function(err , ret){
                if (err)
                        serverError(res , err);
                else{
                        backend.newsDetailsList(ret , function(err,details){
                                res.send(details);
                        }
                        , req.params.lang);
                }      
        });
});

// News info
app.get("/admin/get_news_info/:newsID/:lang", function (req, res) {
	//backend.setSeideBarNews(req.params.newsID , req.params.which);
	backend.newsDetails(utils.fullID(req.params.newsID, req.params.lang) , function(err, ret){
		if (err){	
			res.send(null);
		}else
			res.send(ret);
	});
	
});

// Send the list of available news on bootgrid format
app.post("/admin/listNews" , function(req , res){
	var cur = req.body.current || 1,
		rowCount= req.body.rowCount || 10,
		searchPhrase= req.body.searchPhrase || null;
	
	var ret = {current: cur, rowCount: 10 , searchPhrase : searchPhrase , rows:[] };
	backend.newsList(function(err, list){
		// Active news
		backend.newsDetailsList(list,function(err , ids){
		if (err)
			res.status(500).send("Error on server");
		else{
			var num = 0,
				start = 0;
				// We should use an asyc each for each element since we are using async funcs
				async.each(ids , 
					function(nw , callback){
						backend.newsInFirstPage(nw.id , function(err , firstPage){
						// Filter && Paging
					  	if (!searchPhrase || ((_.includes(nw.mainTitle , searchPhrase) || _.includes(nw.title , searchPhrase) || _.includes(nw.description , searchPhrase))) && (num<cur*rowCount || rowCount==-1)){
							ret.rows.push({
								"mainTitle": nw.mainTitle,
								"title": nw.title,
								"when": nw.when,
								"language": nw.language,
								"description": nw.description,
								"selectedImage": nw.selectedImage,
								"id":nw.id,
								"firstPageSide": firstPage.sideBar || [false,false],
								"firstPageSlideBar": firstPage.slideBar || false,
								"subTitle" : nw.subTitle,
								"video" : nw.video,
								"MobileApp": nw.MobileApp || false
				 			});
				 			num +=1;
				 			callback(null);
				  			}
						});
					} , 
					function(err){
						ret.total = ids.length;
						res.json(ret);
					});
		}
	 });
	});
});

app.get("/admin/remove_news/:id", function (req, res) {
	if (!req.params.id){
		resNotFound(res);
	}else{
		backend.removeNews(req.params.id , LANG , function(err , ret){
			res.send("OK");	
		});
	}
});

//------------------------------ end ADMIN ---------------------------------------------------------



/**
 * Missing route
 */
app.use(function(req , res){
    resNotFound(res);
});

/**
 * SERVER ERROR
 */
app.use(function(err , req , res , next){
	serverError(res , err);
});

// Replies with a not found page
function resNotFound(res){
    res.status(404);
    res.send("400 - NOT FOUND");
}

function serverError(res , err){
	console.log(err);
    res.status(500).send("500 - SERVER ERROR");
}

