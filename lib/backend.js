/**********************************************************************************
* f.invernizzi@libero.it 2015 - 2017
*
*			REDIS and backend INTERACTION	
*
* 17012017 -  Supporto per MobileApp
*
***********************************************************************************/

var configuration = require("../config.js");

var redis = require("redis"),
    async = require('async'),
    utils = require("./util.js"),
    _ = require("lodash"),
    fs = require("fs"),
    htmlToText = require('html-to-text'),
    Log = require('log'),
    htmlToText = require('html-to-text'),
    log = new Log('debug', fs.createWriteStream(configuration.main.logFile));
    
var configuration = require("../config.js");
// Default slideBars elements if needed
var sb_defaults = require('../slideBar.js');
// Gallery elements
var galleryInfo = require('../gallery.js');

var backend = redis.createClient();
backend.on("error", function (err) {
  	log.error("Error " + err);
});

module.exports = {
	newsList: newsList,
	activeNews:activeNews,
	firstPageNewsInfo: firstPageNewsInfo,
	newsDetails: newsDetails,
	newsDetailsList: newsDetailsList,
	updateNews:updateNews,
	sanitizeNewInput:sanitizeNewInput,
	removeNews:removeNews,
	newsInFirstPage:newsInFirstPage,
	setSeideBarNews:setSeideBarNews,
	getSideBarsNews:getSideBarsNews,
	remFromSideBar:remFromSideBar,
	addNewsToSlideBar:addNewsToSlideBar,
	remNewsFromSlideBar:remNewsFromSlideBar,
	newsInSidebar:newsInSidebar,
	setNewsInSlideBar:setNewsInSlideBar,
	addNewsToMobileApp:addNewsToMobileApp,
        remNewsFromMobileApp:remNewsFromMobileApp,
        newsInMobileApp:newsInMobileApp,
	newsMobileAppList: newsMobileAppList,
	getMailsToBeSent: getMailsToBeSent,
	gallery : gallery,
	newMailToBeSent: newMailToBeSent,
	DHsent:DHsent,
	newsSent:newsSent,
	getDL:getDL,
	DLs : DLs,
	addMailToDL: addMailToDL,
	end: function(){backend.end();}
}

/**
* newsList
* return an array containing available newsID with LANG
**/
 function newsList(callback){
	backend.smembers(configuration.ACTIVE_NEWS_LIST, function( err , replies){
		var ret = [];
		for (var i=0 ; i<replies.length ; i++)
			ret.push(utils.fullID(replies[i],configuration.LANG));
		return callback(err , ret);
	})
} 

/**
* activeNews
* return an array containing available newsID WITHOUT LANG
**/
function  activeNews(callback){
	backend.smembers(configuration.ACTIVE_NEWS_LIST , function( err , replies){
		var ret = [];
		for (var i=0 ; i<replies.length ; i++)
			ret.push(replies[i]);
		return callback(err , ret);
	})
}

/**
* newsMobileAppList
* return an array containing available newsID for mobile app WITHOUT LANG
**/
function  newsMobileAppList(callback){
        backend.smembers(configuration.MOBILEAPP , function( err , replies){
                var ret = [];
                for (var i=0 ; i<replies.length ; i++)
                        ret.push(replies[i]);
                return callback(err , ret);
        })
}


/**
*
* returns an obj with all news info  related to first page
*	{sideBar:[sideBar1 , sideBar2], slideBar: ...}
*
*/
function  firstPageNewsInfo(language , mainCallback){
	async.series([
		function(callback){
			backend.smembers(configuration.FIRST_PAGE_NEWS_1 , function(err , replies){
				callback(err, replies[0]);
			})
		},
		function(callback){
			backend.smembers(configuration.FIRST_PAGE_NEWS_2 , function(err , replies){
				callback(err, replies[0]);
			});
		},
		function(callback){
			backend.lrange(configuration.SLIDEBAR , 0 , configuration.NUM_ELEMENTS_SLIDEBAR, function(err , ret){
				callback(err , ret);
			});
		}
	],
	// optional callback
	function(err, results){
		// results 0 and 1 are the sidebar news, 2 is the array of slidebar news
		if (err){
			mainCallback(err , null);
			return false;
		}
		// SIDEBAR
		var side = [];
		[results[0] , results[1]].forEach(function(val , index){
				newsDetails(utils.fullID(val , language), function(error , det){
					if (det){
						det.description = utils.shortenDescription(det.description);
						side.push(det);
					}
				});
			
		});
		 // SLIDEBAR
		var slide = [];
		// Check we have at least NUM_ELEMENTS_SLIDEBAR in slidebar, adding images if needed
		if (results[2].length < configuration.NUM_ELEMENTS_SLIDEBAR){
			var missing = (configuration.NUM_ELEMENTS_SLIDEBAR - results[2].length);
			for (i=0;i<missing;i++)
				slide.push(sb_defaults.defaultElements[i])
		}
		var i = 0;
		results[2].forEach(function(val , index){
			newsDetails(utils.fullID(val , language), function(error , det){
				if (det){
					// We should indicate if wee have a video since the tag changes a bit
					det.isVideo = (!_.isUndefined(det.video) && (det.video!=""));
					slide.unshift(det);
					i +=1;
				}
			});
		});
		mainCallback(err , {sideBar:side , slideBar:slide});
	});
}

// Returns the context json for rendering the gallery
function gallery(){
	return galleryInfo;
}


/**
* news details
* return an object containing details of newsID. If does not exists, return null
* @param newsID the news_id (with trailing LANG)
* @param lang (OPTIONAL) if the lang param is provided, newsID are intende WITH NO LANG
* @return details of a news
**/
function  newsDetails(newsID , callback , lang){
		backend.exists(newsID , function(err, replies){
			if (!replies){
				log.error("News not found:"+newsID);
				return callback(null, null);
			}else{
			if (lang)
				newsID = utils.fullID(newsID , lang);
			 backend.hgetall( newsID , function( err , replies){
			 	replies = enrichNewsDetails(replies);
				return callback(null, replies);
			 });
			}
		});
}

/**
* Return and array with news details
* newsIDList is an array with news_id (WITH LANG)
* if the lang param is provided, newsID are intende WITH NO LANG
**/
function newsDetailsList(newsIDList , callback , lang){
		var completeContext = [];
		// parallel queries to backend and prepare context for template
		async.each(newsIDList, function(newsID , retFunc){
			if (lang)
				newsID = utils.fullID(newsID , lang);
			backend.hgetall( newsID , function( err , replies){
				// FABRIZIO
				if (!replies)
					retFunc();
				else{
					replies = enrichNewsDetails(replies);
					// is News in mobileApp?
					newsInMobileApp(utils.stripLang(newsID), function(err, MA){
						replies.MobileApp = (MA != false) || false ;
						completeContext.unshift(replies);
						retFunc();
					})
			
					//retFunc();
				}
			});
		}, function(err){
			callback(err, completeContext);
		});	
}

// Given a news details obj (typically directly from backend), add extra fields
function enrichNewsDetails(newsDetails){
	if (newsDetails){
		// mainly for social that requires the full HTTP url
		newsDetails.image = configuration.META.host + (newsDetails.selectedImage || "");
		newsDetails.url =  configuration.META.host  + newsDetails.pageUrl;
		newsDetails.shortDescription = utils.shortenDescription(newsDetails.description,10);
		newsDetails.text = htmlToText.fromString(newsDetails.description);
		newsDetails.shortText = htmlToText.fromString(newsDetails.shortDescription);
	}
	return newsDetails;
}

/** 
* Given news info, creates or uptades the backend
* TODO: manage ID received from browser: news to be updated, not created.
**/
function  updateNews(news){
	// sanitises the inputs and generate the newsID
	var newsID = sanitizeNewInput(news);
	// stores values in a new hash
	backend.hmset(utils.fullID(newsID,news.language), {
		title: news.title || "No title",
		subTitle: news.subTitle || "No title",
		when: news.when || "No when",
		selectedImage: news.selectedImage ,
		language: news.language || configuration.LANG,
		description: news.description || "No description",
		id: newsID,
		video: news.video,
		pageUrl: "/news/"+newsID+"/"+news.language
	}, function (err , ret){
		// Add to active news
		if (!err){
			backend.sadd( configuration.ACTIVE_NEWS_LIST, newsID);
		}
	});
	// Mails to be sent?
	var DLs = _.filter( _.keys(news.DLs), function(value , index){return (news.DLs[value].selected == 'true');});
	DLs.forEach(function(dl,index){
		newMailToBeSent(newsID , dl , "HTML");
	});
}

// Given a news input, sanitises it
// returns the internal newsID
// Here the trick about creating a new news or updating an existing one is implemented
// the id is a new one or the one received by get
function  sanitizeNewInput(news){
	var id = null;
	// TODO: for security check if the key already exists
	if (news.id && news.id != ''){
		id = news.id;
	}else
		id = utils.content_ID(news.title) // When can provide any text that will be formatted for an id.
	// TODO:Check the lang is accepted
	
	// Does the image file exists?
	if (!fs.existsSync("./static/"+news.selectedImage)){
		news.selectedImage = configuration.IMG;
	}
	// The News ID
	return id;
}

/**
* Given a newsID, and LANG removes it from backend
**/
function  removeNews(newsID , lang , callback){
	var ID = utils.fullID(newsID , lang);
	backend.srem(configuration.ACTIVE_NEWS_LIST , newsID , function(err , ret){
		if (err)
			log.error(err)
	});
	remFromSideBar(newsID , function(err , ret){
		if (err)
			log.error(err)
	});
	remNewsFromSlideBar(newsID , function(err , ret){
		if (err)
			log.error(err)
	});
	remNewsFromMobileApp(newsID, function(err , ret){
                if (err)
                        log.error(err)
        });)

	backend.del( ID, function(err , ret){
		callback(err , ret);
	});
}

/********************* First page news ********************************/
 
// Given a new ID (no LANG) return a json with first page news settings
 function newsInFirstPage(newsId , mainCallback){
	async.series([
    	function(callback){
        	// First sidebar news?
        	backend.smembers(configuration.FIRST_PAGE_NEWS_1 , function (err , ret){
        		callback(err, newsId == ret[0]);
        	});
    	},
    	function(callback){
        	// Second sidebar news?
        	backend.smembers(configuration.FIRST_PAGE_NEWS_2 , function (err , ret){
        		callback(err, newsId == ret[0]);
        	});
    	},
    	function(callback){
    		// News in slideBar?
    		newsInSidebar(newsId , function(err , ret){
    			callback(err, ret);
    		});
    	}
	],
	// optional callback
	function(err, results){
		mainCallback(err , {sideBar:[results[0] , results[1]] , slideBar:results[2]});
	});
}

/********************* First page SIDEBAR ********************************/

/** 
* Given a newsID (No LANG) and the number of the position (1,2), set in redis the sidebers
* If the position is 1, the first news moves to second position
* If the position is 2, the second position is simply overidden
*/
function  setSeideBarNews(news , which){
	var pos = 1;
	if (!news || (which >2) || (which <1)){
		return false;
	}
	if (which == 1)
		pos = configuration.FIRST_PAGE_NEWS_1;
	else
		pos = configuration.FIRST_PAGE_NEWS_2;
	// stores the new ID
	backend.del(pos , function(err , ret){
		backend.sadd(pos , news);
	});
}

function  getSideBarsNews(which , callback){
	var key = null;
	if (which == 1)
		key = configuration.FIRST_PAGE_NEWS_1;
	else
		key = configuration.FIRST_PAGE_NEWS_2;
	backend.smembers(key , 
		function(err , ret){
				callback(err , ret);
		}
	);
}

// Removes newsID (no LANG) from sideBar
 function  remFromSideBar(newsID , callback){
	backend.srem(configuration.FIRST_PAGE_NEWS_1 , newsID);
	backend.srem(configuration.FIRST_PAGE_NEWS_2 , newsID);
}


/********************* news to be published in MOBILEAPP ********************************/

// Add news to mobileapp
function addNewsToMobileApp(news , callback){
      backend.sadd(configuration.MOBILEAPP , news , function(err , ret){
               callback(err , ret);
      });
}

// Removes a newsID (no LANG) from mobileapp
function  remNewsFromMobileApp(newsID , callback){
                backend.srem(configuration.MOBILEAPP  , 1 , newsID, function(err , ret){
                        callback(err , ret);
                });
}

// callback with err, ret, where ret is true if newsID (no LANG) is in MOBILEAPP
// IF newsID is null, ret is an array containing all the newsID (no lang) in slideBar
function  newsInMobileApp(newsID , callback){
        backend.smembers(configuration.MOBILEAPP , function(err, ret){
		if (typeof ret === 'undefined')	
			 callback(err ,false);
		var isInMobileApp = (ret.indexOf(newsID) != -1);
		callback(err , isInMobileApp);
        });
}


/********************* First page news in SLIDEBAR ********************************/

// Add news to slidebars
// There is a max num of elements in slideBar
function  addNewsToSlideBar(news , callback){
	backend.llen(configuration.SLIDEBAR , function(err , len){
		if (len >= configuration.NUM_ELEMENTS_SLIDEBAR){
			backend.rpop(function(err , ret){
				backend.lpush(configuration.SLIDEBAR , news , function(err , ret){
					callback(err , ret);
				});
			});
		}else{
			backend.lpush(configuration.SLIDEBAR , news , function(err , ret){
					callback(err , ret);
			});
		}
	});
}

// Removes a newsID (no LANG) from slideBar
function  remNewsFromSlideBar(newsID , callback){
		backend.lrem(configuration.SLIDEBAR  , 1 , newsID, function(err , ret){
			callback(err , ret);
		});
}

// callback with err, ret, where ret is true if newsID (no LANG) is in SLIDEBAR
// IF newsID is null, ret is an array containing all the newsID (no lang) in slideBar
function  newsInSidebar(newsID , callback){
	backend.lrange(configuration.SLIDEBAR , 0 , configuration.NUM_ELEMENTS_SLIDEBAR, function(err , ret){
		if (!newsID)
			callback(err , ret);
		else	
			callback(err , (!(_.indexOf(ret, newsID) == -1)));
	});
}

// Given an array containing newsID (no lang) set exactly these as news in the slideBar
// Usefull, for example, for changing order of news
function setNewsInSlideBar(newsIDArray , callback){
	if (_.isArray(newsIDArray)){
		_(newsIDArray).reverse(); // the sadd operation is a push
		//completely remove the list
		backend.del(configuration.SLIDEBAR , function(err, ret){
			if (!err){
				_.each(newsIDArray , function(k){
					backend.lpush(configuration.SLIDEBAR , k , function(err){
						if (err)
							return callback(err);
					});
				});
				callback(null);
			}else
				callback(err);
		});
	}
}

/********************* Mailing LISTS ********************************/
//	 						   	   ---> newsId
// toBeSent ->[Distribute Hashes]+ ---> distribution lists ID in CSV format
//		 					       ---> format
// Quite bad this global, but noOtherWay...
var ditributeInfo = {};
function getMailsToBeSent(mainCallback){
	    // Get active distribute Hashes
        backend.smembers(configuration.mailAgent.TOBESENT , function (err , DHs){
        	async.eachSeries(DHs, getMailDetails, function(err){
    				mainCallback(err , ditributeInfo);
			});
       	});
}

// Given a Distribution Hash, return info needed for the mail to be sent
function getMailDetails(DH , callback){
	// Get Distribute Hash details
        	backend.hgetall(DH , function (err , details){
        		if (!details || err){
        			callback(err || new Error("Error reading DH "+DH+" from backend"));
        		}else{
        			newsDetails(utils.fullID(details.newsID) , function(err , news){
        				getDL(details.dl , function(err , dl){
        					ditributeInfo[DH] ={
        						news:news,
        						dl : dl,
        						format:details.format
        					}
        					callback(err);
        				});
        			});
        		}
        	});
}

// Given a DL ID, returns the list of mails, separated by ";"
// Async, so use the callback
// DL name format: DL:nome:timestamp.
function getDL(DL , callback){
	backend.SMEMBERS(DL , function(err , list){
		if (err){
			log.error(err);
		}	
		callback(err, list.join(";"));
	});
}

// Adds a mail to a DL
function addMailToDL(DL , mail , callback){
	backend.SADD(DL , mail , function(err , ret){
		if (err)
			log.error(err);
		callback(err , ret);
	});
}

// New email to be sent.
// @param newsId : the ID of the news to be sent
// @param dl: distribution list. An array with dls to be used as destinations
// @param format: mail format 
function newMailToBeSent(newsId , dl , format){
	// Some preliminary checks
	// Format, al lin upperCase
	var format = format.toUpperCase();
	if (_.indexOf(["HTML" , "TEXT"] , format) == -1){
		log.error(format + " is not a valid format.");
		return;
	}
	// Does the newsId exist?
	backend.exists(utils.fullID(newsId), function(err , data){
		if (data == 0 || err){
			lor.error("News ID "+newsId+" does not exists");
		}else{
			backend.exists(dl , function(err , data){
				if (data == 0 || err){
					log.error("Distribution List "+dl+" does not exists");
				}else{
					// The name of the DH is take from the dl name, just for clarity
					var dhName = utils.DH_ID(dl);
					// Create the Distribute Hash
					backend.hmset(dhName , "newsID" , newsId , "dl" , dl, "format" , format , function(err , ret){
						if (err)
							log.error(err);
						else{
							log.info("Distribution Hash "+dhName+" created");
							// Add the newly created DH to the toBeSent set
							backend.sadd(configuration.mailAgent.TOBESENT , dhName , function(err , data){
								if (err)
									log.error(err);
								else
									log.info("Distribution Hash "+dhName+" added to "+configuration.mailAgent.TOBESENT )
							});
						}
					});
				
				}
			});
		}
	});
}

// Actions to be performed after sending a DH
function DHsent(DH , callback){
	// Remove DH from toBeSent
	backend.SREM(configuration.mailAgent.TOBESENT , DH , function(err , data){
		if (err){
			log.error(err);
		}else{
			log.info("Distribution Hash "+DH+" removed from set "+configuration.mailAgent.TOBESENT )
		}
		// Log completion time
		backend.HSET(DH , "_SENT_TS_",Date.now() , function(err){callback(err);});
	});	
	
}

// Action to be performed after a news has been sent
// @param newsID : news (without LANG)
// @param DH: Distribution Hash
function newsSent(newsID , DH , callback){
	// Register DH the mail has been set with
	// Read the DH alreay present and updates the field
	backend.HGET(utils.fullID(newsID) , "_SENT_DH_" , function(err , val){
		if (err){
			log.error(err);
		}else{
			if (val && (val != null))
				val += ";";
			else 
				val = "";
			backend.HSET(utils.fullID(newsID) , "_SENT_DH_" , val + DH , function(err){
				if (err)
					log.error(err);
				callback(err);
			});
		}
	});
}

// Returns (callback) an array of Distribution Lists available
function DLs(callback){
	backend.keys("DL:*" , function(err , ret){
		callback(err , ret);
	});
}
