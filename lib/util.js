/**********************************************************************************
* f.invernizzi@libero.it 2015
*
*							UTILITY FUNCTIONS	
*
***********************************************************************************/
var configuration = require("../config.js");

var _ = require("lodash");

module.exports = {
 shortenDescription: function(txt , len){
	if (!len)
		len=configuration.SHORT_NEWS_NUM_WORDS;
	if (!txt)
	 return txt;
	var spl = txt.split(" "),
		ret = "";
	for (i=0 ; i< Math.min(len , spl.length); i++){
		ret += spl[i]+" ";
	}
	ret += "...";
	return ret;
},

 setEnv: function(env , app){
	app.set('env' , env);
	process.env.NODE_ENV = env;
},

// Given the id and the lag returns the complete id
 fullID: function(id , lang){
 	if (!lang)
 		lang = configuration.LANG;
	return id+"_"+lang;
}, 

// Strips the LANG if present off the ID
 stripLang: function(newsID){
	var stripped = newsID.split("_");
	// Has the ID a trailing LANG?
	if (configuration.ACCEPTED_LANG.indexOf(stripped[stripped.length-1]) == -1)
		return newsID; // No valid Lang 
	var ret = "";
	for (var i=0; i<(stripped.length) -1; i++) {
		debugger;
		if (i>0)
			ret += '_';
		ret += stripped[i];
	}
	return(ret);
},

 acceptedLanguage: function(language){
	return ( _.indexOf(configuration.ACCEPTED_LANG, language) != -1);
},
// Given a newsDetails obj, adds info read from configuration file
 newsStaticInfo: function(newsDetails){
 		newsDetails.production = (configuration.main.mode  == "production");
 		newsDetails.pageKeywords = configuration.META.keywords;
 		newsDetails.pageTitle = configuration.META.title;
		newsDetails.pageDescription = configuration.META.description;
 		
 		return newsDetails;
 },	

 /******************************/
 // 		Keys NAMING
 /******************************/

 // Given a name, create a "standard DL ID"
 DL_ID: function(name){
 	return "DL:"+_.camelCase(name)+":"+Date.now();
 },
 // Given a name, create a "standard DH ID "
 DH_ID: function(name){
 	return "DH:"+_.camelCase(name)+":"+Date.now();
 },
 content_ID: function(name){
 	// Adds also a random number for reducing probability of duplicate ids
 	return "NEWS:" + name.replace(/\W+/g, "_") + "_" +this.getRandomInt(0 , 1000);
 },
 
 
  /******************************/
  // 		Various
  /******************************/
  /**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
	getRandomInt: function (min, max) {
    	return Math.floor(Math.random() * (max - min + 1)) + min;
	}
} // module.exports



