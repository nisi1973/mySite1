module.exports = {
	main:{
		/***************************/
		/* development, production */
		/***************************/
		mode:"production" ,
		LISTENPORT : 80,
		processName: "Castelli_di_Lagnasco",
		logDir: '/var/log/',
		logFileRotationFrequency: 'montly',
		logFile: "/var/log/agent.log",
		specialEvent: "Xmas"
	},
	management:{
		logDir: '/var/log/',
		LISTENPORT : 444,
		sslKey:"./ssl/key.pem",
		cert: "./ssl/cert.pem",
		realm: "Castelli di Lagnasco",
    	file: __dirname + "/ssl/users.htpasswd"  
	},
	content:{
		// A questa agent fa prepend di __dirname+
		STATIC_CONTENT_DIR : "/static/"
	},
	mailAgent:{
		host: 'smtps.aruba.it',
 	   	port: 465,
		name: "www.castellidilagnasco.it",
    		auth: {
       		 	user: 'fabrizio@castellidilagnasco.it',
       	 		pass: 'Dacord2014'
    		},
    		TOBESENT: "ml_toBeSent" ,
	        template:  "./views/mail_news.handlebars",
       	 	from: 'news@castellidilagnasco.it',
       	 	to: 'news@castellidilagnasco.it',
       	 	mode: "oneTime", // daemon , oneTime
       	 	checkPeriod : 10000, // Only for daemon mode
		logFile: "/var/log/mailAgent.log"
	},
	META:{
		// Default meta info
		keywords:"Torino turismo, castelli piemonte, piemonte, Saluzzo, Marchesato, Tapparelli, D'azeglio, castelli di Lagnasco, castello di Lagnasco",
		title: "Castelli di Lagnasco",
		description: "Castello di Lagnasco un percorso fra una delle più interessanti dimore storiche del Piemonte e del Marchesato, a due passi da Saluzzo e Torino, nelle domeniche tra marzo e ottobre.",
		image: "http://www.castellidilagnasco.it/images/slider1.jpg",
		url: "http://www.castellidilagnasco.it",
		host: "http://www.castellidilagnasco.it"
	},
	 
	 SHORT_NEWS_NUM_WORDS : 20,
	 BAD_REQUEST : 400,
	// To be managed with cookies
	 LANG : "IT", // Default language
	 IMG : "/images/logo.png",
	 UPLOADS_DIR : "./static/uploads",
	 NOT_FOUND_NEWS_ID : "not_found", // If not found, use this news id
	 DEFAULT_NEWS_ID : "not_found", 
	 SERVER_ERROR_NEWS_ID : "server_error", // Server error page
	 ACTIVE_NEWS_LIST : "news_active",
	 FIRST_PAGE_NEWS_1 : "first_page_news_1", // Prima news sulla spalla destra della prima pagina
	 FIRST_PAGE_NEWS_2 : "first_page_news_2", // Seconda news sulla spalla destra della prima pagina
	 SLIDEBAR : "slidebar" , //Name of the set containing news in the slidebar
	 MOBILEAPP : "mobileapp" , //Name of the set containing news to be published to mobile app
	 NUM_ELEMENTS_SLIDEBAR : 5, // Min number of elements to be kept in SLIDEBAR 
	// Accepted languages
	 ACCEPTED_LANG: ["IT" , "EN"]
}

